﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Commands.CreateWorkItemInBoard
{
    public class CreateBugInBoardCommand : Command
    {
        private readonly ITeamRepository teamRepository;

        /// <author>Mihail Popov</author>
        public CreateBugInBoardCommand(IList<string> commandParameters, IDatabase database, IWriter writer, ITeamRepository teamRepository)
            : base(commandParameters, database,writer)
        {
            this.teamRepository = teamRepository;
        }

        public override string Execute()
        {
            //a team name and board name must be provided for a bug to be added in a board, because
            //there could be teams with equal board names.
            if (this.CommandParameters.Count < 6)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER,
                    "team, board, work item title, work item description, priority, severity, steps to reproduce"));
            }

            var team = Database.GetTeam(this.CommandParameters[0]);
            var board =  teamRepository.GetBoard(team, this.CommandParameters[1]);
           // var board = team.GetBoard(this.CommandParameters[1]);

            if (!Enum.TryParse(this.CommandParameters[4], out Priority priority))
            {
                throw new ArgumentException(ErrorStrings.INVALID_PRIORITY);
            }

            if (!Enum.TryParse(this.CommandParameters[5], out BugSeverity severity))
            {
                throw new ArgumentException(ErrorStrings.INVALID_BUG_SEVERITY);
            }

            var stepsToReproduce = this.CommandParameters.Skip(6).ToList();

            stepsToReproduce.ForEach(param => param.Trim());

            //board.CheckWorkItemExistance(typeof(Bug), this.CommandParameters[2]);
            board.CheckWorkItemExistance("Bug", this.CommandParameters[2]);

            var bug = this.Factory
                      .CreateBugInBoard(
                       title: this.CommandParameters[2],
                       description: this.CommandParameters[3],
                       priority: priority,
                       severity: severity,
                       stepsToReproduce: stepsToReproduce
                      );

            board.AddWorkItem(bug);

            bug.RecordTeamAndBoard(team, board);

            //this.Database.WorkItems.Add(bug);

            this.Database.AddWorkItem(bug);

            string result = string
                .Format(SuccessfullStrings.WORK_ITEM_ADDED_IN_BOARD, "Bug", bug.Title, team.Name, board.Name);

            this.Writer.SetPrintColor(StringType.Success);
            return result;
        }
    }
}
