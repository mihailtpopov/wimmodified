﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Commands.CreateWorkItem
{
    /// <author>Mihail Popov</author>
    
    public class CreateStoryInBoardCommand : Command
    {
        public CreateStoryInBoardCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 6)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER,
                    "team, board, work item title, work item description, priority and size"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);

            if (!Enum.TryParse(this.CommandParameters[4], out Priority priority))
            {
                throw new ArgumentException(ErrorStrings.INVALID_PRIORITY);
            }

            if (!Enum.TryParse(this.CommandParameters[5], out StorySize size))
            {
                throw new ArgumentException(ErrorStrings.INVALID_STORY_SIZE);
            }

            //board.CheckWorkItemExistance(typeof(Story), this.CommandParameters[2]);
            board.CheckWorkItemExistance("story", this.CommandParameters[2]);

            var story = this.Factory.CreateStoryInBoard(
                title: this.CommandParameters[2],
                description: this.CommandParameters[3],
                priority: priority,
                size: size
                );

            board.AddWorkItem(story);

            story.RecordTeamAndBoard(team, board);

            //this.Database.WorkItems.Add(story);

            this.Database.AddWorkItem(story);

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(
                SuccessfullStrings.WORK_ITEM_ADDED_IN_BOARD, "Story", story.Title, team.Name, board.Name);
        }
    }
}
