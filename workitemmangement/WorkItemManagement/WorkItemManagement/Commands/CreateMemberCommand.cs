﻿using System;
using System.Linq;
using System.Collections.Generic;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Models;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    public class CreateMemberCommand : Command
    {
        public CreateMemberCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            :base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: createperson "Name"
            if (this.CommandParameters.Count!=1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "member name"));
            }

            string name = this.CommandParameters[0];

            this.Database.CheckMemberExistance(name);

            Member member = this.Factory.CreateMember(name);

            this.Database.AddMember(member);

            //this.Writer.SetPrintColor(StringType.Success);
            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(SuccessfullStrings.CREATED_MEMBER, member);
        }
    }
}
