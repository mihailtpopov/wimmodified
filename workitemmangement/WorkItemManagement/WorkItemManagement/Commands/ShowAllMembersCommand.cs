﻿using WorkItemManagement.Commands.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Commands
{
    /// <author>Mihail Popov</author>
    public class ShowAllMembersCommand : Command
    {
        public ShowAllMembersCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.Database.Members.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_MEMBERS_YET);
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("List of all members:");
            sb.AppendLine("Name");
            sb.AppendLine(string.Join(Environment.NewLine, this.Database.Members.Select(m => m.Name).ToList()));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
