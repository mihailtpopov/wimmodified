﻿
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Mihail Popov</author>
    public class FilterTeamItemsByStatusCommand : Command
    {
        private readonly ITeamWorkItemsFilter filter;

        public FilterTeamItemsByStatusCommand(IList<string> commandParameters, IDatabase database, IWriter writer, ITeamWorkItemsFilter filter) 
            : base(commandParameters, database,writer)
        {
            this.filter = filter;
        }

        public override string Execute()
        {
            if (!(this.CommandParameters.Count == 3 || this.CommandParameters.Count == 4))
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team and/or board, work item type and status"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            IBoard board=null;
            string workItemType;
            string statusAsString;

            if(this.CommandParameters.Count==4)
            {
                board = team.GetBoard(this.CommandParameters[1]);
                workItemType = this.CommandParameters[2];
                statusAsString = this.CommandParameters[3];
            }
            else
            {
                workItemType = this.CommandParameters[1];
                statusAsString = this.CommandParameters[2];
            }

            IEnumerable<IWorkItem> filtratedItems = workItemType.ToLower() switch
            {
                "bug" => Enum.TryParse(statusAsString, out BugStatus status)
                         ? filter.GetFilteredWorkItemsBy(team, status, board)
                         : throw new ArgumentException(ErrorStrings.INVALID_BUG_STATUS),

                "story" => Enum.TryParse(statusAsString, out StoryStatus status)
                           ? filter.GetFilteredWorkItemsBy(team, status, board)
                           : throw new ArgumentException(ErrorStrings.INVALID_STORY_STATUS),

                "feedback" => Enum.TryParse(statusAsString, out FeedbackStatus status)
                              ? filter.GetFilteredWorkItemsBy(team, status, board)
                              : throw new ArgumentException(ErrorStrings.INVALID_FEEDBACK_STATUS),

                _ => throw new ArgumentException(ErrorStrings.INVALID_WORK_ITEM_TYPE)
            };

            string workItemTypeInHeader = workItemType switch
            {
                "bug" => "bugs",
                "story" => "stories",
                "feedback" => "feedbacks",
                _ => ""
            };

            string headerRow;
            if (board == null)
            {
                headerRow = $"List of {workItemTypeInHeader} in team \"{team.Name}\":";
            }
            else
            {
                headerRow = $"List of {workItemTypeInHeader} in board \"{board.Name}\" in team \"{team.Name}\":";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(headerRow);
            sb.Append(string.Join(Environment.NewLine, filtratedItems));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
