﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Commands.ChnageFeedbackCommands
{
    /// <author>Georgi Stefanov</author>
    class ChangeFeedbackStatusCommand : Command
    {
        public ChangeFeedbackStatusCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: changefeedbackpriority Team Board Feedback NewStatus
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, feedback title and new status"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var feedback = board.GetFeedbackByTitle(this.CommandParameters[2]);

            if (!Enum.TryParse(this.CommandParameters[3], out FeedbackStatus status))
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_WORK_ITEM_LABEL,
                    "Feedback",
                    "status ",
                    string.Join("/", Enum.GetValues(typeof(FeedbackStatus)).Cast<FeedbackStatus>().ToList())
                    ));
            }

            var currentValue = feedback.Status;
            var newValue = status;
            if (currentValue == newValue)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Status", currentValue);
            }
            else
            {
                FeedbackStatus current = feedback.Status;

                feedback.ChangeStatus(status);

                board.EventLog.AddEvent(new Event(
                    string.Format(EventStrings.WI_PROPERTY_CHANGED, "Feedback", feedback.Title, "status", current, feedback.Status)));

                this.Writer.SetPrintColor(StringType.Success);
                return string.Format(SuccessfullStrings.WORK_ITEM_CHANGED, "Feedback", $"{feedback.Title}", "status", newValue);
            }
        }
    }
}
