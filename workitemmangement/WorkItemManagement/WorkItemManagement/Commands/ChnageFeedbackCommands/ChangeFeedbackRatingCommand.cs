﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;

namespace WorkItemManagement.Commands.ChnageFeedbackCommands
{
    /// <author>Georgi Stefanov</author>
    class ChangeFeedbackRatingCommand : Command
    {
        public ChangeFeedbackRatingCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: changefeedbackrating Team Board Feedback NewRating
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, feedback title and new rating"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var feedback = board.GetFeedbackByTitle(this.CommandParameters[2]);

            if (!int.TryParse(this.CommandParameters[3], out int rating))
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_RATING_TYPE));
            }

            var currentValue = feedback.Rating;
            var newValue = rating;
            if (currentValue == newValue)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Rating", currentValue);
            }
            else
            {
                int current = feedback.Rating;

                feedback.ChangeRating(newValue);

                board.EventLog.AddEvent(new Event(
                    string.Format(EventStrings.WI_PROPERTY_CHANGED, "Feedback", feedback.Title, "rating", current, feedback.Rating)));

                this.Writer.SetPrintColor(StringType.Success);
                return string.Format(SuccessfullStrings.WORK_ITEM_CHANGED, "Feedback", $"{feedback.Title}", "rating", newValue);
            }
        }
    }
}
