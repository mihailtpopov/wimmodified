﻿using WorkItemManagement.Commands.Abstracts;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core;
using System.Text;
using System.Linq;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Mihail Popov</author>
    public class ShowAllTeamBoardsCommand : Command
    {
        public ShowAllTeamBoardsCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            if (team.Boards.Count==0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            const int col1 = -11;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Team \"{team.Name}\" boards list:");
            //sb.AppendLine($"{"Name",col1}");
            sb.AppendLine(string.Join(
                Environment.NewLine,
                team.Boards.OrderBy(b => b.Name).Select(b => $"{b.Name,col1}").ToList()));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
