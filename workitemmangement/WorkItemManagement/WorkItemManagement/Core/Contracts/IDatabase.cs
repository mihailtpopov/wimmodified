﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Abstracts;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    public interface IDatabase : IWorkItemsFilter, IAssignedWorkItemsFilter
    {
        IReadOnlyList<ITeam> Teams { get; }

        IReadOnlyList<IMember> Members { get; }

        IReadOnlyList<IWorkItem> WorkItems { get; }

        void AddMember(IMember member);

        void AddTeam(ITeam team);

        void AddWorkItem(IWorkItem workItem);

        ITeam GetTeam(string teamName);

        IMember GetMember(string memberName);


        string ListAllWorkItems();

        void CheckMemberExistance(string memberName);

        void CheckTeamExistance(string team);

        /// <summary>
        /// Gets all work items of the specified type in the Database
        /// </summary>
        /// <param name="type">The type to search and filter</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IWorkItem> GetWorkItemsFilteredByType(Type type);

        /// <summary>
        /// Gets all work items in the Database sorted by title
        /// </summary>
        /// <param name="sortType">The sorting type (asc/desc)</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IWorkItem> GetWorkItemsSortedByTitle(string sortType);

        /// <summary>
        /// Gets all bugs and stories in the Database sorted by priority
        /// </summary>
        /// <param name="sortType">The sorting type (asc/desc)</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IBugStory> GetWorkItemsSortedByPriority(string sortType);

        /// <summary>
        /// Gets all bugs in the Database sorted by severity
        /// </summary>
        /// <param name="sortType">The sorting type (asc/desc)</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IBug> GetWorkItemsSortedBySeverity(string sortType);

        /// <summary>
        /// Gets all stories in the Database sorted by size
        /// </summary>
        /// <param name="sortType">The sorting type (asc/desc)</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IStory> GetWorkItemsSortedBySize(string sortType);

        /// <summary>
        /// Gets all feedbacks in the Database sorted by rating
        /// </summary>
        /// <param name="sortType">The sorting type (asc/desc)</param>
        /// <returns></returns>
        /// <author>Georgi Stefanov</author>
        IEnumerable<IFeedback> GetWorkItemsSortedByRating(string sortType);

    }
}
