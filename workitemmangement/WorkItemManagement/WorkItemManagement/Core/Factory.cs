﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Core
{
    public class Factory : IFactory
    {
        private static IFactory instance;
        public static IFactory Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new Factory();
                }

                return instance;
            }
        }
        
        public Member CreateMember(string name)
        {
           return new Member(name);
        }

        public Team CreateTeam(string name)
        {
            return new Team(name);
        }

        public IWorkItem CreateBugInBoard(string title, string description, Priority priority, BugSeverity severity, List<string> stepsToReproduce)
        {
            var bug = new Bug(title, description, priority, severity, stepsToReproduce);
            return bug;
        }

        public IWorkItem CreateFeedbackInBoard(string title, string description)
        {
            return new Feedback(title, description);
        }

        public IWorkItem CreateStoryInBoard(string title, string description, Priority priority, StorySize size)
        {
            return new Story(title, description, priority, size);
        }

        public Comment CreateComment(string commentText, IMember author)
        {
            var comment = new Comment(commentText, author);
            return comment;
        }

        public Board CreateBoard(string name)
        {
            var board = new Board(name);
            return board;
        }
    }
}
