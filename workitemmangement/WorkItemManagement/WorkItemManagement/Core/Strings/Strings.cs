﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core.Strings
{
    public static class Strings
    {
        public const string DASHES = "-------";
        public const string PROPERTY_NOT_CHANGED = "{0} is already {1}";
    }
}
