﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core.Strings
{
    public static class EventStrings
    {
        //team strings
        public const string MEMBER_ADDED = "Member {0} added.";
        public const string NAME_CHANGED = "{0} \"{1}\" has changed its name to \"{2}\".";
        public const string TEAM_CREATED = "Team created.";
        public const string BOARD_ADDED = "Board \"{0}\" added.";
        //new Event($"Board {board.Name} successfully added to team {this.Name}")

        //member strings
        public const string MEMBER_REGITERED = "Member registered.";

        public const string MEMBER_ADDED_IN_TEAM = "Added in team {0}.";

        public const string WORKITEM_ASSIGNED = "Assigned {0} \"{1}\".";
        public const string WORKITEM_UNASSIGNED = "Unassigned {0} \"{1}\".";
        public const string ADD_COMMENT = "Added comment to {0} \"{1}\", in board \"{2}\" in team \"{3}\"";

        //board strings
        public const string BOARD_CREATED = "Board created.";
        public const string WORKITEM_ADDED_TO_BOARD = "{0} \"{1}\" added.";
        public const string WI_PROPERTY_CHANGED = "{0} \"{1}\" {2} changed from {3} to {4}.";
        // Bug "..." status changed from XXX to YYY.
        public const string MEMBER_ADD_COMMENT = "Member {0} added comment to {1} \"{2}\".";


        //work item strings
        public const string WORK_ITEM_CREATED = "{0} created.";
        public const string COMMENT_ADDED = "{0} added comment.";
        public const string PROPERTY_CHANGED = "{0} changed from {1} to {2}.";
        public const string ASSIGNEE_ADDED = "Added assignee {0}.";
        public const string ASSIGNEE_REMOVED = "Removed assignee {0}.";

    }
}
