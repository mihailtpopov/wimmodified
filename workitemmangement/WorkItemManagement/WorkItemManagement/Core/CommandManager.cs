﻿using WorkItemManagement.Commands;
using WorkItemManagement.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Commands.Contracts;
using WorkItemManagement.Commands.ListWorkItemsCommands;
using WorkItemManagement.Commands.CreateWorkItemInBoard;
using WorkItemManagement.Commands.CreateWorkItem;
using WorkItemManagement.Commands.ChangeStoryCommands;
using WorkItemManagement.Commands.AssignUnassignWorkItem;
using WorkItemManagement.Commands.ChangeBugCommands;
using WorkItemManagement.Commands.ChnageFeedbackCommands;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Core
{
    public class CommandManager : ICommandManager
    {
        private readonly ITeamRepository teamRepository;
        private readonly ITeamWorkItemsFilter teamWiFilter;

        public CommandManager(ITeamRepository teamRepository, ITeamWorkItemsFilter teamWiFilter)
        {
            this.teamRepository = teamRepository;
            this.teamWiFilter = teamWiFilter;
        }

        public ICommand ParseCommand(string commandLine, IDatabase database, IWriter writer)
        {
            var lineParameters = commandLine
                .Trim()
                .Split('/', StringSplitOptions.RemoveEmptyEntries);

            string commandName = lineParameters[0];
            List<string> commandParameters = lineParameters.Skip(1).ToList();

            return commandName.ToLower() switch
            {
                "createmember" => new CreateMemberCommand(commandParameters, database, writer),
                "createbug" => new CreateBugInBoardCommand(commandParameters, database, writer, teamRepository),
                "createfeedback" => new CreateFeedbackInBoardCommand(commandParameters,database, writer),
                "createstory" => new CreateStoryInBoardCommand(commandParameters, database, writer),
                "createteam" => new CreateTeamCommand(commandParameters, database, writer),
                "createboard" => new CreateNewTeamBoardCommand(commandParameters, database, writer),

                "addmembertoteam" => new AddPersonToTeamCommand(commandParameters, database, writer),
                "addworkitemcomment" => new AddWorkItemCommentCommand(commandParameters, database, writer),

                "assignworkitem" => new AssignWorkItemCommand(commandParameters, database, writer),
                "unassignworkitem" => new UnassignWorkItemCommand(commandParameters, database, writer),

                "changestorypriority" => new ChangeStoryPriorityCommand(commandParameters, database, writer),
                "changestorysize" => new ChangeStorySizeCommand(commandParameters, database, writer),
                "changestorystatus" => new ChangeStoryStatusCommand(commandParameters, database, writer),
                "changebugpriority" => new ChangeBugPriorityCommand(commandParameters, database, writer),
                "changebugseverity" => new ChangeBugSeverityCommand(commandParameters, database, writer),
                "changebugstatus" => new ChangeBugStatusCommand(commandParameters, database, writer),
                "changefeedbackstatus" => new ChangeFeedbackStatusCommand(commandParameters, database, writer),
                "changefeedbackrating" => new ChangeFeedbackRatingCommand(commandParameters, database, writer),

                "showallmembers" => new ShowAllMembersCommand(commandParameters, database, writer),
                "showallteams" => new ShowAllTeams(commandParameters, database, writer),
                "showallteammembers" => new ShowAllTeamMembersCommand(commandParameters, database, writer),
                "showallteamboards" => new ShowAllTeamBoardsCommand(commandParameters, database, writer),
                "showallteamsactivity" => new ShowTeamsActivityCommand(commandParameters, database, writer),
                "showboardactivity" => new ShowBoardActivity(commandParameters, database, writer),
                "showmemberactivity" => new ShowMemberActivity(commandParameters, database, writer),

                "listallworkitems" => new ListAllWorkItemsCommand(commandParameters, database, writer),
                "listallteamworkitems" => new ListAllTeamWorkItemsCommand(commandParameters, database, writer),
                "listallboardworkitems" => new ListAllBoardItemsCommand(commandParameters, database, writer),
                "filterworkitemsbystatus" => new FilterAllWorkItemsByStatus(commandParameters, database, writer),
                "filterteamworkitemsbystatus" => new FilterTeamItemsByStatusCommand(commandParameters, database, writer),
                "filterbystatusandassignee" => new FilterByStatusAndAssigneeCommand(commandParameters, database, writer),
                "filterbyworkitemtype" => new FilterByWorkItemType(commandParameters, database, writer),
                "sortworkitemsby" => new SortWorkItemsByCommand(commandParameters, database, writer),

                _ => throw new InvalidOperationException("Command does not exist")
            };
        }
    }
}
