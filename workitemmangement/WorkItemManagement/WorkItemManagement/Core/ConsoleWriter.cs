﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Core
{
    public class ConsoleWriter : IWriter
    {
        //private const ConsoleColor baseColor = ConsoleColor.Gray;
        private readonly ConsoleColor baseColor;
        private ConsoleColor printColor;
        public ConsoleWriter()
        {
            this.baseColor = Console.ForegroundColor;
        }

        public void SetPrintColor(StringType stringType)
        {
            this.printColor = stringType switch
            {
                StringType.Error => this.printColor = ConsoleColor.Red,
                StringType.Success => this.printColor = ConsoleColor.Green,
                StringType.List => this.printColor = ConsoleColor.Cyan,
                StringType.Activity => this.printColor = ConsoleColor.Blue,
                StringType.Neutral => this.printColor=ConsoleColor.Yellow,
                _ => this.printColor = this.baseColor
            };

        }

        public void Print(string commandResult)
        {
            Console.ForegroundColor = this.printColor;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(commandResult);
            sb.AppendLine("####################");
            Console.WriteLine(sb.ToString().Trim());
            //Console.ResetColor();
            Console.ForegroundColor = baseColor;
        }
    }
}
