﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IMember
    {
        public int ID { get; }

        public string Name { get; }

        public IReadOnlyList<ITeam> Teams { get; }

        public IEventLog EventLog { get; }

        public IReadOnlyList<IWorkItem> WorkItems { get; }

        public void AssignItem(IBugStory item);

        public void UnassignItem(IBugStory item);

        public void AddTeam(ITeam team);

        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status);

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status);

        public IEnumerable<IBug> GetAllBugs();

        public IEnumerable<IStory> GetAllStories();
    }
}
