﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    public interface ITeamWorkItemsFilter
    {
        public List<IFeedback> GetFilteredWorkItemsBy(ITeam team, FeedbackStatus status, IBoard board = null);

        public List<IBug> GetFilteredWorkItemsBy(ITeam team, BugStatus status, IBoard board = null);

        public List<IStory> GetFilteredWorkItemsBy(ITeam team, StoryStatus status, IBoard board = null);
    }
}
