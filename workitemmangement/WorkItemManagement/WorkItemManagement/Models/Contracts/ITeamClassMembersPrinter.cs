﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    public interface ITeamClassMembersPrinter
    {
        public string ShowMembers(ITeam team);

        public string ShowActivity(ITeam team);

        public string ShowBoards(ITeam team);

        public string ListAllItems(ITeam team);
    }
}
