﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Georgi Stefanov</author>
    public interface IBugStory : IWorkItem
    {
        Priority Priority { get; }

        IReadOnlyList<IMember> Assignees { get; }

        /// <summary>
        /// Adds a member to the assignees list of the work item
        /// </summary>
        /// <param name="assignee">The member to add</param>
        void AddAssignee(IMember assignee);

        /// <summary>
        /// Removes a member from the assignees list of the work item
        /// </summary>
        /// <param name="assignee">The member to add</param>
        void RemoveAssignee(IMember assignee);

    }
}
