﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    public interface ITeamRepository
    {
        IMember GetMember(ITeam team, string memberName);

        IBoard GetBoard(ITeam team, string boardName);

        void AddBoard(ITeam team, IBoard board);

        void CheckBoardExistance(ITeam team, string boardName);

        void CheckMemberExistance(ITeam team, string memberName)
    }
}
