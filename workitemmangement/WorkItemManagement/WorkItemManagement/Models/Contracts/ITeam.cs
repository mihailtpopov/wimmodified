﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface ITeam
    { 
        public string Name { get; }

        public IReadOnlyList<IMember> Members { get; }

        public IReadOnlyList<IBoard> Boards { get; }

        public IEventLog EventLog { get; }

        public int ID { get; }
    }
}
