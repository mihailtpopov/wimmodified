﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IEvent
    {
        public DateTime EventTime { get; }

        public string EventMessage { get; }
    }
}
