﻿using WorkItemManagement.Core;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Abstracts;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Models.WorkItems
{
    /// <author>Georgi Stefanov</author>
    public class Story : WorkItem, IStory
    {
        private List<IMember> assignedMembers = new List<IMember>();

        public Story(string title, string description,  Priority priority, StorySize size)
            : base(title, description)
        {
            this.Status = StoryStatus.NotDone;
            this.Priority = priority;
            this.Size = size;

            EventLog.AddEvent(new Event(string.Format(EventStrings.WORK_ITEM_CREATED, "Story")));
        }


        public StoryStatus Status { get; private set; }

        public Priority Priority { get; private set; }

        public StorySize Size { get; private set; }

        public IReadOnlyList<IMember> Assignees { get => this.assignedMembers;}

        public void AddAssignee(IMember assignee)
        {

            if (this.Assignees != null)
            {
                if (!this.assignedMembers.Contains(assignee))
                {
                    this.assignedMembers.Add(assignee);
                    EventLog.AddEvent(new Event(string.Format(EventStrings.ASSIGNEE_ADDED, assignee.Name)));
                }

            }        
        }

        public void RemoveAssignee(IMember assignee)
        {
            if (this.assignedMembers.Contains(assignee))
            {
                this.assignedMembers.Remove(assignee);
                EventLog.AddEvent(new Event(string.Format(EventStrings.ASSIGNEE_REMOVED, assignee.Name)));
            }
        }

        public void ChangeSize(StorySize size)
        {
            var current = this.Size;
            this.Size = size;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Size", current, this.Size)));
        }

        public void ChangeStatus(StoryStatus status)
        {
            var current = this.Status;
            this.Status = status;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Status", current, this.Status)));
        }

        public void ChangePriority(Priority priority)
        {
            var current = this.Priority;
            this.Priority = priority;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Priority", current, this.Priority)));
        }
    }
}
