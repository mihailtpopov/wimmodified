﻿using WorkItemManagement.Core;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Core.Strings;
using System.Linq;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Models
{
    /// <author>Mihail Popov</author>
    public class Member : IMember, IAssignedWorkItemsFilter
    {
        private const int NAME_MIN_LENGTH = 5;
        private const int NAME_MAX_LENGTH = 15;
        private static int id;
        private List<IWorkItem> workItems;
        private List<ITeam> teams;
        public Member(string name)
        {
            if (name == null || name.Length < NAME_MIN_LENGTH || name.Length > NAME_MAX_LENGTH)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_NAME_LENGTH, "Member", NAME_MIN_LENGTH, NAME_MAX_LENGTH));
            }

            this.Name = name;
            this.workItems = new List<IWorkItem>();
            this.teams = new List<ITeam>();
            this.EventLog = new EventLog();
            this.ID = ++id;

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_REGITERED)));
        }

        public int ID { get; }
        public string Name { get; }
        public IReadOnlyList<ITeam> Teams { get => this.teams; }

        public IEventLog EventLog { get; }

        public IReadOnlyList<IWorkItem> WorkItems { get => this.workItems; }

        public override bool Equals(object obj)
        {
            if (obj is Member)
            {
                Member otherMember = (Member)obj;

                if (this.Name == otherMember.Name)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }

        public void AssignItem(IBugStory item)
        {
            if (this.WorkItems.Contains(item))
            {
                throw new ArgumentException(string.Format(ErrorStrings.ALREADY_ASSIGNED_ITEM, this.Name));
            }

            this.workItems.Add(item);

            this.EventLog.AddEvent(
                new Event(string.Format(EventStrings.WORKITEM_ASSIGNED, item.GetType().Name, item.Title)));

            item.AddAssignee(this);
        }

        public void UnassignItem(IBugStory item)
        {
            if (!this.WorkItems.Contains(item))
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_SUCH_ITEM_ASSIGNED, this.Name));
            }

            this.workItems.Remove(item);

            this.EventLog.AddEvent(
                new Event(string.Format(EventStrings.WORKITEM_UNASSIGNED, item.GetType().Name, item.Title)));

            item.RemoveAssignee(this);
        }

        public void AddTeam(ITeam team)
        {
            if (this.Teams.Contains(team))
            {
                throw new ArgumentException(ErrorStrings.MEMBER_ALREADY_IN_TEAM);
            }

            this.teams.Add(team);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED_IN_TEAM, team.Name)));
        }

        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
        {
            var stories = this.GetAllStories();

            IEnumerable<IStory> filteredStories = stories.Where(item => item.Status == status);

            if (filteredStories.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.STORIES_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredStories;
        }

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
        {
            var bugs = this.GetAllBugs();

            IEnumerable<IBug> filteredBugs = bugs.Where(item => item.Status == status);

            if (filteredBugs.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.BUGS_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredBugs;
        }

        public IEnumerable<IBug> GetAllBugs()
        {
            var bugs = this.WorkItems.OfType<IBug>();

            if (bugs.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NOT_ASSIGNED_WORKITEMS, "bugs"));
            }

            return bugs;
        }

        public IEnumerable<IStory> GetAllStories()
        {
            var stories = this.WorkItems.OfType<IStory>();

            if (stories.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NOT_ASSIGNED_WORKITEMS, "stories"));
            }

            return stories;
        }
    }
}
