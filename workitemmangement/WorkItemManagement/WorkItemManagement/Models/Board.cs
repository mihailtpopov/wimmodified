﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    public class Board : IBoard
    {
        private const int NAME_MIN_LENGTH = 5;
        private const int NAME_MAX_LENGTH = 10;
        private string name;
        private List<IWorkItem> workItems;
        private static int id;
        public Board(string name)
        {
            this.ValidateName(name);

            this.name = name;
            this.EventLog = new EventLog();
            this.workItems = new List<IWorkItem>();
            this.ID = ++id;


            this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_CREATED, this.Name)));
        }

        public string Name
        {
            get => this.name;

            set
            {
                this.ValidateName(value);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.NAME_CHANGED, "Board", this.Name, value)));

                this.name = value;
            }
        }

        public EventLog EventLog { get; }

        public IReadOnlyList<IWorkItem> WorkItems { get => this.workItems; }

        public int ID { get; }

        public override string ToString()
        {
            return this.Name;
        }

        public void AddWorkItem(IWorkItem item)
        {
            if (WorkItems.Contains(item))
            {
                throw new ArgumentException(ErrorStrings.DUPLICATE_WORK_ITEM);
            }

            workItems.Add(item);

            this.EventLog.AddEvent(
            new Event(string.Format(EventStrings.WORKITEM_ADDED_TO_BOARD, item.GetType().Name, item.Title)));
        }

        public string ShowActivity()
        {
            return string.Join(Environment.NewLine, this.EventLog.History);
        }

        public IEnumerable<IFeedback> GetAllFeedbacks()
        {
            var feedbacks = this.WorkItems.OfType<IFeedback>();
            return feedbacks;
        }

        public IEnumerable<IBug> GetAllBugs()
        {
            var bugs = this.WorkItems.OfType<IBug>();
            return bugs;
        }

        public IEnumerable<IStory> GetAllStories()
        {
            var stories = this.WorkItems.OfType<IStory>();
            return stories;
        }

        public IWorkItem GetWorkItemByTypeAndTitle(string type, string title)
        {
            return type.ToLower() switch
            {
                "bug" => this.WorkItems.Where(item => (item is IBug && item.Title == title)).FirstOrDefault(),
                "feedback" => this.WorkItems.Where(item => (item is IFeedback && item.Title == title)).FirstOrDefault(),
                "story" => this.WorkItems.Where(item => (item is IStory && item.Title == title)).FirstOrDefault(),
                _ => throw new ArgumentException(string.Format(ErrorStrings.INVALID_WORK_ITEM_TYPE))
            }
            ?? throw new ArgumentException(string.Format(
                ErrorStrings.INVALID_WORK_ITEM,
                type[0].ToString().ToUpper() + type.Substring(1),
                title));
            //add one more placeholder for work item type in const INVALID_WORK_ITEM
        }

        public void CheckWorkItemExistance(string type, string title)
        {
            bool doesExist = type.ToLower() switch
            {
                "bug" => this.WorkItems
                .Where(item => item is IBug)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                "feedback" => this.WorkItems
                .Where(item => item is IFeedback)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                "story" => this.WorkItems
                .Where(item => item is IStory)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                _ => throw new ArgumentException(ErrorStrings.INVALID_WORK_ITEM_TYPE)
            };

            if (doesExist)
            {
                throw type.ToLower() switch
                {
                    "bug" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Bug", this.Name)),
                    "feedback" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Feedback", this.Name)),
                    "story" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Story", this.Name)),
                    _ => new ArgumentException(),
                };
            }
        }

        public IFeedback GetFeedbackByTitle(string feedbackTitle)
        {
            var story = WorkItems
                .Where(item => (item.Title == feedbackTitle && item is IFeedback))
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Feedback", $"\"{feedbackTitle}\""));

            return (IFeedback)story;
        }

        public IBug GetBugByTitle(string bugTitle)
        {
            var story = WorkItems
                .Where(item => (item.Title == bugTitle && item is IBug))
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Bug", $"\"{bugTitle}\""));

            return (IBug)story;
        }

        /// <summary>
        ///  Get concrete story by given title
        /// </summary>
        /// <param name="storyTitle">the title to check for</param>
        /// <returns>object of type IStory</returns>
        /// <author>Mihail Popov</author>
        public IStory GetStoryByTitle(string storyTitle)
        {
            var story = WorkItems
                .Where(item => (item.Title == storyTitle && item is IStory))
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Story", $"\"{storyTitle}\""));

            return (IStory)story;
        }

        private void ValidateName(string name)
        {
            if (name.Length < NAME_MIN_LENGTH || name.Length > NAME_MAX_LENGTH)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_NAME_LENGTH, "Board", NAME_MIN_LENGTH, NAME_MAX_LENGTH));
            }
        }

        public string ListAllItems()
        {
            return string.Join(Environment.NewLine, this.WorkItems.Select(wi => $"{wi.GetType().Name,-10}{wi}"));
        }
    }
}
