﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    class TeamClassMembersPrinter
    {
        public string ShowMembers(ITeam team)
        {
            if (team.Members.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_MEMBERS_YET);
            }
            
            return string.Join(Environment.NewLine, team.Members);
        }

        public string ShowActivity(ITeam team)
        {
            return string.Join(Environment.NewLine, team.EventLog.History);
        }

        public string ShowBoards(ITeam team)
        {
            if (team.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            return string.Join(Environment.NewLine, team.Boards);
        }

        public string ListAllItems(ITeam team)
        {
            if (team.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            List<IWorkItem> allBoardsItems = new List<IWorkItem>();

            team.Boards.ToList().ForEach(board => allBoardsItems.AddRange(board.WorkItems));

            if (allBoardsItems.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_WORKITEMS_IN_TEAM_YET);
            }

            return string.Join(Environment.NewLine, allBoardsItems.Select(wi => $"{wi.GetType().Name,-10}{wi}"));
        }
    }
}
