﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    class TeamRepository : ITeamRepository
    {

        public void AddTeammate(ITeam team, IMember member)
        {
            if (team.Members.Contains(member))
            {
                throw new ArgumentException(string.Format(ErrorStrings.MEMBER_EXIST, member.Name, team.Name));
            }

            //this.members.Add(member);
            member.AddTeam(team);

            team.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED, member.Name)));
        }

        public IMember GetMember(ITeam team, string memberName)
        {
            var member = team.Members
                .Where(member => member.Name == memberName)
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_MEMBER, memberName, team.Name));

            return member;
        }

        public IBoard GetBoard(ITeam team, string boardName)
        {
            var board = team.Boards
                .Where(board => board.Name == $"{boardName}")
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_BOARD, boardName, team.Name));

            return board;
        }

        public void AddBoard(ITeam team, IBoard board)
        {
            if (team.Boards.Contains(board))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
            }

            //team.boards.Add(board);

            team.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED, board.Name)));
        }

        public void CheckBoardExistance(ITeam team, string boardName)
        {
            if (team.Boards.Select(board => board.Name).Contains(boardName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", boardName));
            }
        }

        public void CheckMemberExistance(ITeam team, string memberName)
        {
            if (team.Members.Select(member => member.Name).Contains(memberName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Member", memberName));
            }
        }
    }
}
