﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models
{
    class TeamWorkItemsFilter : ITeamWorkItemsFilter
    {
        public List<IFeedback> GetFilteredWorkItemsBy(ITeam team, FeedbackStatus status, IBoard board = null)
        {
            List<IFeedback> filteredFeedbackItems = new List<IFeedback>();

            if (board != null)
            {
                var boardFeedbacks = board.GetAllFeedbacks();

                if (boardFeedbacks.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "feedbacks"));
                }

                filteredFeedbackItems = boardFeedbacks
                   .Where(item => item.Status == status)
                   .ToList();

                if (filteredFeedbackItems.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredFeedbackItems;
            }

            if (team.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            team.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.GetAllFeedbacks().Count() != 0)
                    {
                        filteredFeedbackItems
                       .AddRange(
                            board.WorkItems
                            .Where(item => item is IFeedback)
                            .Select(item => item as IFeedback)
                            .Where(item => item.Status == status)
                            .ToList()
                              );
                    }
                });

            if (filteredFeedbackItems.Count() == 0)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.FEEDBACK_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredFeedbackItems;
        }

        public List<IBug> GetFilteredWorkItemsBy(ITeam team, BugStatus status, IBoard board = null)
        {
            List<IBug> filteredBugItems = new List<IBug>();

            if (board != null)
            {
                var boardBugs = board.GetAllBugs();

                if (boardBugs.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "bugs"));
                }

                filteredBugItems = boardBugs
                   .Where(item => item.Status == status)
                   .ToList();

                if (filteredBugItems.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredBugItems;
            }

            if (team.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            team.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.GetAllBugs().Count() != 0)
                    {
                        filteredBugItems
                        .AddRange(
                            board.WorkItems
                            .Where(item => item is IBug)
                            .Select(item => item as IBug)
                            .Where(item => item.Status == status)
                            );
                    }
                });

            if (filteredBugItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.BUGS_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredBugItems;
        }

        public List<IStory> GetFilteredWorkItemsBy(ITeam team, StoryStatus status, IBoard board = null)
        {
            List<IStory> filteredStoryItems = new List<IStory>();

            if (board != null)
            {
                var boardStories = board.GetAllStories();

                if (boardStories.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "stories"));
                }

                filteredStoryItems = boardStories
                   .Where(item => item.Status == status)
                   .ToList();

                if (filteredStoryItems.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredStoryItems;
            }

            if (team.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            team.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.WorkItems.Where(item => item is IStory).Count() != 0)
                    {
                        filteredStoryItems
                        .AddRange(
                            board.WorkItems
                            .Where(item => item is IStory)
                            .Select(item => item as IStory)
                            .Where(item => item.Status == status)
                            .ToList()
                            );
                    }
                });

            if (filteredStoryItems.Count() == 0)
            {
                throw new ArgumentException(
                                    string.Format(ErrorStrings.STORIES_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredStoryItems;
        }
    }
}
