﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    /// <author>Mihail Popov</author>
    public class Event : IEvent
    {
        private string eventMessage;

        public Event(string eventMessage)
        {
            this.EventMessage = eventMessage;
            this.EventTime = DateTime.Now;
        }

        public DateTime EventTime { get; }

        public string EventMessage
        {
            get => this.eventMessage;

            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Event message can not be empty");
                }

                this.eventMessage = value;
            }
        }

        public override string ToString()
        {
            string formattedDate = this.EventTime.ToString("yyyy/MM/dd");
            string formattedTime = this.EventTime.ToString("HH:mm:ss");

            return $"{formattedDate} {formattedTime} - {this.EventMessage}";
        }
    }
}
