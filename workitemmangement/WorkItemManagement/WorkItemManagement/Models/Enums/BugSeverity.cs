﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Enums
{
    public enum BugSeverity
    {
        Minor = 1,
        Major = 2,
        Critical = 3
    }
}
