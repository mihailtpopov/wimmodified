﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    public enum FeedbackStatus
    {
        New,
        Unscheduled,
        Scheduled,
        Done
    }
}
