﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models
{
    /// <author>Mihail Popov</author>
    public class Team : ITeam
    {
        private const int NAME_MIN_LENGTH = 2;
        private const int NAME_MAX_LENGTH = 20;
        private string name;
        private List<IBoard> boards;
        private List<IMember> members;
        private static int id;

        public Team(string name)
        {
            this.ValidateName(name);

            this.name = name;
            this.boards = new List<IBoard>();
            this.members = new List<IMember>();
            this.EventLog = new EventLog();
            this.ID = ++id;

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.TEAM_CREATED)));
        }

        public string Name
        {
            get => this.name;

            set
            {
                this.ValidateName(value);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.NAME_CHANGED, name, value)));

                this.name = value;
            }
        }

        public IReadOnlyList<IMember> Members { get => this.members; }

        public IReadOnlyList<IBoard> Boards { get => this.boards; }

        public IEventLog EventLog { get; }

        public int ID { get; }

        public override string ToString()
        {
            return this.Name;
        }

        private void ValidateName(string name)
        {
            if (name == null || name.Length < NAME_MIN_LENGTH || name.Length > NAME_MAX_LENGTH)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_NAME_LENGTH, "Team", NAME_MIN_LENGTH, NAME_MAX_LENGTH));
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Team)
            {
                Team otherMember = (Team)obj;

                if (this.Name == otherMember.Name)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


    }
}