﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class ShowActivity_Should
    {
        [TestMethod]
        public void ReturnCorrectResult()
        {
            //Arrange
            var fakeTeam = new FakeTeam_Ver2();
            var fakeBoard = new FakeBoard();

            //Act
            fakeTeam.AddBoard(fakeBoard);

            //Assert
            Assert.AreEqual(fakeTeam.ShowActivity(), string.Join(Environment.NewLine, fakeTeam.EventLog.History));
        }
    }

    class FakeTeam_Ver2 : ITeam
    {
        private List<IBoard> boards = new List<IBoard>();
        public string Name => throw new NotImplementedException();

        public IReadOnlyList<IMember> Members => throw new NotImplementedException();

        public IReadOnlyList<IBoard> Boards => throw new NotImplementedException();

        public IEventLog EventLog { get; } = new EventLog();

        public int ID => throw new NotImplementedException();

        public void AddBoard(IBoard board)
        {
            if (this.boards.Contains(board))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
            }

            this.boards.Add(board);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED, board.Name)));
        }

        public void AddTeammate(IMember member)
        {
            throw new NotImplementedException();
        }

        public void CheckBoardExistance(string boardName)
        {
            throw new NotImplementedException();
        }

        public void CheckMemberExistance(string memberName)
        {
            throw new NotImplementedException();
        }

        public IBoard GetBoard(string boardName)
        {
            throw new NotImplementedException();
        }

        public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public IMember GetMember(string memberName)
        {
            throw new NotImplementedException();
        }

        public string ListAllItems()
        {
            throw new NotImplementedException();
        }

        public string ShowActivity()
        {
            return string.Join(Environment.NewLine, this.EventLog.History);
        }

        public string ShowBoards()
        {
            throw new NotImplementedException();
        }

        public string ShowMembers()
        {
            throw new NotImplementedException();
        }
    }
}
