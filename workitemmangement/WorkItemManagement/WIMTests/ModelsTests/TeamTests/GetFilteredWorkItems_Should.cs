﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{ 
    [TestClass]
    public class GetFilteredWorkItems_Should
    {
        [TestMethod]
        public void ThrowWhen_BoardHasNoSuchWorkItemType()
        {
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetFilteredWorkItemsBy(BugStatus.Active, fakeBoard));
        }

        [TestMethod]
        public void ThrowWhen_BoardDoesNotResidesInTeam()
        {
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetFilteredWorkItemsBy(BugStatus.Active, fakeBoard));
        }

        [TestMethod]
        public void ThrowWhen_BoardHasNoItemsWithSuchStatus()
        {
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeBoard.AddWorkItem(new FakeBug());

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetFilteredWorkItemsBy(BugStatus.Active, fakeBoard));
        }

        [TestMethod]
        public void ReturnCollectionCorrectly()
        {
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeBoard.AddWorkItem(new FakeBug());
            fakeTeam.AddBoard(fakeBoard);

            Assert.IsInstanceOfType(fakeTeam.GetFilteredWorkItemsBy(BugStatus.Fixed, fakeBoard), typeof(IEnumerable<IBug>));
        }

       [TestMethod]
       public void ThrowWhen_NoBoardsAddedToTeam()
        {
            var fakeTeam = new FakeTeam();

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetFilteredWorkItemsBy(BugStatus.Active, null));
        }

        [TestMethod]
        public void ThrowWhen_NoTeamBoardHasItemWithSuchStatus()
        {
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeBoard.AddWorkItem(new FakeBug());
            fakeTeam.AddBoard(fakeBoard);

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetFilteredWorkItemsBy(BugStatus.Active, null));
        }
        class FakeTeam : ITeam
        {
            private List<IBoard> boards = new List<IBoard>();
            public string Name => throw new NotImplementedException();

            public IReadOnlyList<IMember> Members => throw new NotImplementedException();

            public IReadOnlyList<IBoard> Boards { get => this.boards; }

            public IEventLog EventLog { get; } = new EventLog();

            public int ID => throw new NotImplementedException();

            public void AddBoard(IBoard board)
            {
                if (this.boards.Contains(board))
                {
                    throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
                }

                this.boards.Add(board);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED, board.Name)));
            }

            public void AddTeammate(IMember member)
            {
                throw new NotImplementedException();
            }

            public void CheckBoardExistance(string boardName)
            {
                throw new NotImplementedException();
            }

            public void CheckMemberExistance(string memberName)
            {
                throw new NotImplementedException();
            }

            public IBoard GetBoard(string boardName)
            {
                throw new NotImplementedException();
            }

            public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
            {
                List<IBug> filteredBugItems = new List<IBug>();

                if (board != null)
                {
                    var boardBugs = board.GetAllBugs();

                    if (boardBugs.Count() == 0)
                    {
                        throw new ArgumentException(
                            string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "bugs"));
                    }

                    filteredBugItems = boardBugs
                       .Where(item => item.Status == status)
                       .ToList();

                    if(filteredBugItems.Count==0)
                    {
                        throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                    }

                    return filteredBugItems;
                }

                if (this.Boards.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
                }

                this.Boards.ToList()
                    .ForEach(board =>
                    {
                        if (board.GetAllBugs().Count() != 0)
                        {
                            filteredBugItems
                            .AddRange(
                                board.WorkItems
                                .Where(item => item is IBug)
                                .Select(item => item as IBug)
                                .Where(item => item.Status == status)
                                );
                        }
                    });

                if (filteredBugItems.Count() == 0)
                {
                    throw new ArgumentException(string.Format(ErrorStrings.BUGS_WITH_STATUS_NOT_FOUND, status));
                }

                return filteredBugItems;
            }

            public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public IMember GetMember(string memberName)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }

            public string ShowBoards()
            {
                throw new NotImplementedException();
            }

            public string ShowMembers()
            {
                throw new NotImplementedException();
            }
        }

        class FakeBug : IBug
        {
            public BugStatus Status { get; } = BugStatus.Fixed;

            public IReadOnlyList<string> StepsToReproduce => throw new NotImplementedException();

            public BugSeverity Severity => throw new NotImplementedException();

            public Priority Priority => throw new NotImplementedException();

            public IReadOnlyList<IMember> Assignees => throw new NotImplementedException();

            public int ID => throw new NotImplementedException();

            public string Title => "testWorkItemTitle";

            public string Description => throw new NotImplementedException();

            public IReadOnlyList<Comment> Comments => throw new NotImplementedException();

            public IEventLog EventLog => throw new NotImplementedException();

            public void AddAssignee(IMember assignee)
            {
                throw new NotImplementedException();
            }

            public void AddComment(Comment comment)
            {
                throw new NotImplementedException();
            }

            public void ChangePriority(Priority priority)
            {
                throw new NotImplementedException();
            }

            public void ChangeSeverity(BugSeverity severity)
            {
                throw new NotImplementedException();
            }

            public void ChangeStatus(BugStatus status)
            {
                throw new NotImplementedException();
            }

            public void RecordTeamAndBoard(ITeam team, IBoard board)
            {
                throw new NotImplementedException();
            }

            public void RemoveAssignee(IMember assignee)
            {
                throw new NotImplementedException();
            }
        }

        class FakeBoard : IBoard
        {
            private List<IWorkItem> workItems = new List<IWorkItem>();
            public string Name => "testBoardName";

            public EventLog EventLog { get; } = new EventLog();

            public IReadOnlyList<IWorkItem> WorkItems { get => this.workItems; }

            public int ID => throw new NotImplementedException();

            public void AddWorkItem(IWorkItem item)
            {
                if (this.workItems.Contains(item))
                {
                    throw new ArgumentException(ErrorStrings.DUPLICATE_WORK_ITEM);
                }

                workItems.Add(item);

                this.EventLog.AddEvent(
                new Event(string.Format(EventStrings.WORKITEM_ADDED_TO_BOARD, item.GetType().Name, item.Title)));
            }

            public void CheckWorkItemExistance(string type, string title)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                var bugs = this.workItems.OfType<IBug>();
                return bugs;
            }

            public IEnumerable<IFeedback> GetAllFeedbacks()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IBug GetBugByTitle(string bugTitle)
            {
                throw new NotImplementedException();
            }

            public IFeedback GetFeedbackByTitle(string feedbackTitle)
            {
                throw new NotImplementedException();
            }

            public IStory GetStoryByTitle(string storyTitle)
            {
                throw new NotImplementedException();
            }

            public IWorkItem GetWorkItemByTypeAndTitle(string type, string title)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }
        }
    }
}
