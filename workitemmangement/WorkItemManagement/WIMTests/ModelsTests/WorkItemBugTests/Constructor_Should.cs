﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WIMTests.ModelsTests.WorkItemBugTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void AssignCorrectTitle()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(title, testBug.Title);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("tit")]
        [DataRow("A successor to the programming language B, C was originally developed ...")]
        public void ThrowExceptionWhenInvalidTitle(string title)
        {
            //Arrange
            //string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bug(title, description, priority, severity, stepsToReproduce));
        }

        [TestMethod]
        public void AssignCorrectDescription()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(description, testBug.Description);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("descr")]
        [DataRow("A successor to the programming language B, C was originally developed at Bell Labs by " +
            "Dennis Ritchie between 1972 and 1973 to construct utilities running on Unix. It was applied to " +
            "re-implementing the kernel of the Unix operating system.[6] During the 1980s, C gradually gained popularity." +
            " It has become one of the most widely used programming languages,[7][8] with C compilers from various vendors" +
            " available for the majority of existing computer architectures and operating systems. C has been standardized by" +
            " the ANSI since 1989 (ANSI C) and by the International Organization for Standardization (ISO).")]
        public void ThrowExceptionWhenInvalidDescription(string description)
        {
            //Arrange
            string title = "this is a normal title";
            //string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bug(title, description, priority, severity, stepsToReproduce));
        }

        [TestMethod]
        public void AssignCorrecInitialStatus()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act
            var testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(BugStatus.Active, testBug.Status);
        }

        [TestMethod]
        public void AssignCorrectPriority()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(priority, testBug.Priority);
        }

        [TestMethod]
        public void AssignCorrectSeverity()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(severity, testBug.Severity);
        }

        [TestMethod]
        public void AssignCorrectStepsToReproduce()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { "1", "2", "3" };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(stepsToReproduce, testBug.StepsToReproduce);
        }

        [TestMethod]
        public void EventLogWriteAtConstruction()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { "1", "2", "3" };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.AreEqual(1, testBug.EventLog.History.Count);
        }

        [TestMethod]
        public void InitializeCorrectAssigneesListType()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { "1", "2", "3" };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.IsInstanceOfType(testBug.Assignees, typeof(IList<IMember>));
        }

        [TestMethod]
        public void InitializeCorrectCommentsListType()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { "1", "2", "3" };

            //Act
            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Assert
            Assert.IsInstanceOfType(testBug.Comments, typeof(IReadOnlyList<Comment>));
        }

    }
}
