﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.FakeModelsForCommandsTests
{
    public class FakeMember : IMember
    {
        public int ID => throw new NotImplementedException();

        public string Name => "testAssignee";

        public IReadOnlyList<ITeam> Teams => throw new NotImplementedException();

        public IEventLog EventLog => throw new NotImplementedException();

        public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

        public void AddTeam(Team team)
        {
            throw new NotImplementedException();
        }

        public void AssignItem(IBugStory item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBug> GetAllBugs()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStory> GetAllStories()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
        {
            throw new NotImplementedException();
        }

        public void UnassignItem(IBugStory item)
        {
            throw new NotImplementedException();
        }
    }
}
