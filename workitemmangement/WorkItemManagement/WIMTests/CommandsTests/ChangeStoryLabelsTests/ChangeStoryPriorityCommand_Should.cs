﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.CommandsTests.CreateWorkItemTests;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands.ChangeStoryCommands;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WIMTests.CommandsTests.ChangeStoryLabelsTests
{
    [TestClass]
    public class ChangeStoryPriorityCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_InvalidParametersNumber()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "lastButNotEnough"
            };

            new ChangeStoryPriorityCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team does not exists")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newPriority",
            };

            new ChangeStoryPriorityCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Board does not exists")]
        public void ThrowWhen_BoardDoesNotExists()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newPriority",
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryPriorityCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Story with such title does not exists")]
        public void ThrowWhen_StoryTitleDoesNotExists()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newPriority",
            };

            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryPriorityCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid story priority")]
        public void ThrowWhen_StoryPriorityIsInvalid()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "invalidPriority",
            };

            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeBoard.AddWorkItem(new FakeWorkItem());
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryPriorityCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }
    }
}
