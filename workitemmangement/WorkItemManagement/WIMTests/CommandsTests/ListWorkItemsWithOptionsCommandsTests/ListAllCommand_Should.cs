﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands.ListWorkItemsCommands;

namespace WIMTests.CommandsTests.ListWorkItemsWithOptionsCommandsTests
{
    [TestClass]
    public class ListAllCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_InvalidParametersNumber()
        {
            //Arrange 
            List<string> commandParams = new List<string>() { "wrongParam" };

            //Act & Assert
            new ListAllWorkItemsCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No workitems found")]
        public void ThrowWhen_NoWorkItems()
        {
            //Arrange 
            List<string> commandParams = new List<string>();

            //Act & Assert
            new ListAllWorkItemsCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }
    }
}
